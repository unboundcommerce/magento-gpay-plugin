<?php
/*
Purpose : Reading Input Config Json File
*/
namespace UnboundCommerce\GooglePay\Service\Gateway\cybersource\client\Authentication\Util;
use UnboundCommerce\GooglePay\Service\Gateway\cybersource\client\Authentication\Core\MerchantConfiguration as MerchantConfiguration;
class PropertiesUtil
{

	public function readConfig($fileName)
	{
		 if(file_exists($fileName)){
		 	$configConst = file_get_contents($fileName);
		 	return json_decode($configConst);
		 }else{
		 	return null;
		 }

	}

}
